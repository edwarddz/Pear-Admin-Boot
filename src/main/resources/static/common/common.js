var dataType = [
    {"typeCode":0,"typeName":"债券基金","sizeMin":2,"sizeMax":100},
    {"typeCode":1,"typeName":"股票基金","sizeMin":2,"sizeMax":100},
    {"typeCode":2,"typeName":"混合基金","sizeMin":2,"sizeMax":200},
    {"typeCode":3,"typeName":"商品基金","sizeMin":5,"sizeMax":500}
]