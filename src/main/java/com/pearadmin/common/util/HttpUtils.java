package com.pearadmin.common.util;

import com.google.gson.JsonObject;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.NameValuePair;
import org.apache.http.ParseException;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;


/**
 * http请求工具类，封装了http、https的常用请求方法
 *
 * @author LuoJie
 * @date 2018-03-13
 */
public class HttpUtils {

    private final static Logger LOGGER = LoggerFactory.getLogger(HttpUtils.class);

    // 私有的构造方法，防止被外部实例化
    private HttpUtils() {
    }

    /**
     * 发送http协议get请求
     *
     * @param url    请求地址
     * @param params 参数
     * @return 返回响应值
     */
    public static String doGet(String url, Map<String, String> params) {
        if (StringUtils.isBlank(url)) {
            return null;
        }

        HttpGet get = null;
        CloseableHttpClient client = null;
        CloseableHttpResponse response = null;
        try {
            get = createHttpGet(appendParams(url, params));
            get.setConfig(initCfg());
            get.setHeader("Host","api.trade.eastmoney.com");
            get.setHeader("User-Agent","Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36");
            String referer = "http://fundf10.eastmoney.com/jjjz_"+ params.get("fundCode")+".html";
            get.setHeader("Referer",referer);

            client = HttpClients.createDefault();
            response = client.execute(get);

            return answer(response);
        } catch (Exception e) {
            LOGGER.error(String.format("GET请求[%s]出现异常，参数：%s", url, params), e);
            return null;
        } finally {
            close(get, client, response);
        }
    }

    /**
     * 发送http协议get请求
     *
     * @param url    请求地址
     * @param params 参数
     * @return 返回字节数组
     */
    public static byte[] doGetToByteArray(String url, Map<String, String> params) {
        if (StringUtils.isBlank(url)) {
            return null;
        }

        HttpGet get = null;
        CloseableHttpClient client = null;
        CloseableHttpResponse response = null;
        try {
            get = createHttpGet(appendParams(url, params));
            get.setConfig(initCfg());

            client = HttpClients.createDefault();
            response = client.execute(get);

            return answerByteArray(response);
        } catch (Exception e) {
            LOGGER.error(String.format("GET请求[%s]出现异常，参数：%s", url, params), e);
            return null;
        } finally {
            close(get, client, response);
        }
    }


    /**
     * 基于表单数据提交的http协议post请求
     *
     * @param url    请求地址
     * @param params 参数
     * @return 返回响应值
     */
    public static String doPost(String url, Map<String, String> params) {
        if (StringUtils.isBlank(url)) {
            return null;
        }

        HttpPost post = null;
        CloseableHttpClient client = null;
        CloseableHttpResponse response = null;
        try {
            post = createHttpPost(url);
            post.setConfig(initCfg());
            post.setEntity(createUrlEncodedFormEntity(mapToNameValuePairs(params)));

            client = HttpClients.createDefault();
            response = client.execute(post);

            return answer(response);
        } catch (Exception e) {
            LOGGER.error(String.format("POST请求[%s]出现异常，参数：%s", url, params), e);
            return null;
        } finally {
            close(post, client, response);
        }
    }

    /**
     * 基于表单数据提交的http协议post请求
     *
     * @param url    请求地址
     * @param params 参数
     * @return 返回字节数组
     */
    public static byte[] doPostToByteArray(String url, Map<String, String> params) {
        if (StringUtils.isBlank(url)) {
            return null;
        }

        HttpPost post = null;
        CloseableHttpClient client = null;
        CloseableHttpResponse response = null;
        try {
            post = createHttpPost(url);
            post.setConfig(initCfg());
            post.setEntity(createUrlEncodedFormEntity(mapToNameValuePairs(params)));

            client = HttpClients.createDefault();
            response = client.execute(post);

            return answerByteArray(response);
        } catch (Exception e) {
            LOGGER.error(String.format("POST请求[%s]出现异常，参数：%s", url, params), e);
            return null;
        } finally {
            close(post, client, response);
        }
    }

    /**
     * 基于Json数据提交的http协议post请求
     *
     * @param url    请求地址
     * @param params 参数
     * @return 返回响应值
     */
    public static String doPostByJson(String url, Map<String, String> params) {
        if (StringUtils.isBlank(url)) {
            return null;
        }

        HttpPost post = null;
        CloseableHttpClient client = null;
        CloseableHttpResponse response = null;
        try {
            post = createHttpPost(url);
            post.setConfig(initCfg());
            post.setEntity(createStringEntity(mapToJson(params)));

            client = HttpClients.createDefault();
            response = client.execute(post);

            return answer(response);
        } catch (Exception e) {
            LOGGER.error(String.format("POST请求[%s]出现异常，参数：%s", url, params), e);
            return null;
        } finally {
            close(post, client, response);
        }
    }

    /**
     * 基于Json数据提交的http协议post请求
     *
     * @param url    请求地址
     * @param params 参数
     * @return 返回响应值
     */
    public static String doPostByJson(String url, JsonObject params) {
        if (StringUtils.isBlank(url)) {
            return null;
        }

        HttpPost post = null;
        CloseableHttpClient client = null;
        CloseableHttpResponse response = null;
        try {
            post = createHttpPost(url);
            post.setConfig(initCfg());
            post.setEntity(createStringEntity(params));

            client = HttpClients.createDefault();
            response = client.execute(post);

            return answer(response);
        } catch (Exception e) {
            LOGGER.error(String.format("POST请求[%s]出现异常，参数：%s", url, params), e);
            return null;
        } finally {
            close(post, client, response);
        }
    }

    /**
     * Get请求拼接参数
     *
     * @param url    请求地址
     * @param params 参数Map
     * @return 返回完整的Get请求路径
     * @throws UnsupportedEncodingException
     */
    public static String appendParams(String url, Map<String, String> params) throws UnsupportedEncodingException {
        if (Objects.isNull(params) || params.isEmpty()) {
            return url;
        }

        final StringBuilder paramStr = new StringBuilder();
        for (Entry<String, String> entry : params.entrySet()) {
            paramStr.append("&").append(entry.getKey()).append("=").append(URLEncoder.encode(entry.getValue(), "utf-8"));
        }

        return url.concat("?").concat(paramStr.substring(1));
    }


    private static HttpGet createHttpGet(String url) {
        return new HttpGet(url);
    }

    private static HttpPost createHttpPost(String url) {
        return new HttpPost(url);
    }

    private static RequestConfig initCfg() {
        return RequestConfig.custom().setSocketTimeout(30000).setConnectTimeout(8000).build();
    }

    private static List<NameValuePair> mapToNameValuePairs(Map<String, String> params) {
        List<NameValuePair> list = new ArrayList<>();
        if (Objects.nonNull(params)) {
            params.forEach((key, value) -> list.add(new BasicNameValuePair(key, value)));
        }

        return list;
    }

    private static JsonObject mapToJson(Map<String, String> params) {
        JsonObject paramsJson = new JsonObject();
        if (Objects.nonNull(params)) {
            params.forEach((key, value) -> paramsJson.addProperty(key, value));
        }

        return paramsJson;
    }

    private static StringEntity createStringEntity(JsonObject params) {
        return new StringEntity(Objects.nonNull(params) ? params.toString() : "", ContentType.APPLICATION_JSON);
    }

    private static UrlEncodedFormEntity createUrlEncodedFormEntity(List<NameValuePair> pairs) {
        return new UrlEncodedFormEntity(pairs, StandardCharsets.UTF_8);
    }

    private static String answer(CloseableHttpResponse response) throws ParseException, IOException {
        return EntityUtils.toString(response.getEntity(), StandardCharsets.UTF_8);
    }

    private static byte[] answerByteArray(CloseableHttpResponse response) throws IOException {
        return EntityUtils.toByteArray(response.getEntity());
    }

    private static void close(HttpRequestBase request, CloseableHttpClient client, CloseableHttpResponse response) {
        if (Objects.nonNull(request)) {
            request.releaseConnection();
        }

        if (Objects.nonNull(client)) {
            try {
                client.close();
            } catch (IOException e) {
                // ignore...
            }
        }

        if (Objects.nonNull(response)) {
            try {
                response.close();
            } catch (IOException e) {
                // ignore...
            }
        }
    }
} 
