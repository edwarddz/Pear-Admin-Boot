package com.pearadmin.trade.service;

import com.github.pagehelper.PageInfo;
import com.pearadmin.common.web.domain.request.PageDomain;
import com.pearadmin.common.web.domain.response.Result;
import com.pearadmin.trade.pojo.TradeConfig;

/**
* @author lidezhi
* @description 针对表【trade_config(交易爬虫配置表)】的数据库操作Service
* @createDate 2023-07-19 18:44:14
*/
public interface TradeConfigService{

    Result fundInfoData(TradeConfig tradeConfig);

    PageInfo<TradeConfig> page(TradeConfig param, PageDomain pageDomain);

}
