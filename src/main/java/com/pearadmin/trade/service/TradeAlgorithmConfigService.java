package com.pearadmin.trade.service;

import com.github.pagehelper.PageInfo;
import com.pearadmin.common.web.domain.request.PageDomain;
import com.pearadmin.trade.pojo.TradeAlgorithmConfig;

/**
* @author lidezhi
* @description 针对表【trade_algorithm_config】的数据库操作Service
* @createDate 2024-07-24 14:50:12
*/
public interface TradeAlgorithmConfigService {

    PageInfo<TradeAlgorithmConfig> page(TradeAlgorithmConfig param, PageDomain pageDomain);

}
