package com.pearadmin.trade.service;

import com.github.pagehelper.PageInfo;
import com.pearadmin.common.web.domain.request.PageDomain;
import com.pearadmin.trade.pojo.TradeCrawlRecord;
import com.pearadmin.trade.pojo.dto.TradeCrawlRecordDTO;

/**
 * @Author: lidezhi
 * @DATE: 2024/11/18 16:16
 * @Description:
 * @Version 1.0
 */
public interface TradeCrawlRecordService {

    PageInfo<TradeCrawlRecordDTO> page(TradeCrawlRecord param, PageDomain pageDomain);
}
