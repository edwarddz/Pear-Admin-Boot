package com.pearadmin.trade.service;

import com.github.pagehelper.PageInfo;
import com.pearadmin.common.web.domain.request.PageDomain;
import com.pearadmin.common.web.domain.response.Result;
import com.pearadmin.trade.pojo.TradeConfig;
import com.pearadmin.trade.pojo.TradeRecord;
import com.pearadmin.trade.pojo.dto.TradeRecordDTO;

/**
* @author lidezhi
* @description 针对表【trade_record(交易记录表)】的数据库操作Service
* @createDate 2023-07-20 15:18:22
*/
public interface TradeRecordService{

    Result fundTradeData(TradeConfig tradeConfig);

    PageInfo<TradeRecordDTO> page(TradeRecord param, PageDomain pageDomain);

}
