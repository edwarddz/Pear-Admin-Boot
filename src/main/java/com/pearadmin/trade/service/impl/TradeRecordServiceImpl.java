package com.pearadmin.trade.service.impl;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.ObjectUtil;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.pearadmin.common.web.domain.request.PageDomain;
import com.pearadmin.common.web.domain.response.Result;
import com.pearadmin.trade.constant.CrawledPlaceEnum;
import com.pearadmin.trade.constant.StatusConstant;
import com.pearadmin.trade.mapper.TradeCrawlRecordMapper;
import com.pearadmin.trade.pojo.TradeCrawlRecord;
import com.pearadmin.trade.pojo.TradeConfig;
import com.pearadmin.trade.pojo.TradeRecord;
import com.pearadmin.trade.pojo.dto.TradeRecordDTO;
import com.pearadmin.trade.service.TradeRecordService;
import com.pearadmin.trade.mapper.TradeRecordMapper;
import com.pearadmin.common.util.HttpUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.*;

/**
* @author lidezhi
* @description 针对表【trade_record(交易记录表)】的数据库操作Service实现
* @createDate 2023-07-19 18:43:28
*/
@Slf4j
@Service
public class TradeRecordServiceImpl implements TradeRecordService{

    @Resource
    private TradeRecordMapper tradeRecordMapper;

    @Resource
    private TradeCrawlRecordMapper crawlRecordMapper;
    @Override
    public Result fundTradeData(TradeConfig tradeConfig) {
        Result result = new Result();

        //爬取之前先查询已有的记录
        TradeRecord lastTradeRecord = tradeRecordMapper.selectBylast(tradeConfig.getTradeCode());

        String url ="http://api.fund.eastmoney.com/f10/lsjz";
        Map<String, String> params = new HashMap<>();
        String callback = "jQuery18308909743577296265_1618718938738";
        params.put("callback",callback);
        params.put("fundCode", tradeConfig.getTradeCode());
        int pageSize = 100;
        params.put("pageSize", pageSize + "");

        long start = System.currentTimeMillis();
        int pageIndex = 1;
        //统计爬取数量
        int count = 0;
        do{
            params.put("pageIndex", pageIndex+"");
            params.put("_", new Date().getTime()+"");

            String str = HttpUtils.doGet(url, params);
            str = str.replace(callback,"");
            str = str.replace("(","");
            str = str.replace(")","");

            log.info("爬取成功，当前时间::::" + DateUtil.formatDateTime(new Date()) );
            JSONObject jsonObject = JSONObject.parseObject(str);
            JSONArray jsonArray = jsonObject.getJSONObject("Data").getJSONArray("LSJZList");
            if(jsonArray == null){
                break;
            }
            List<TradeRecord> tradeRecords = new ArrayList<TradeRecord>();
            for (Object x:jsonArray) {
                TradeRecord tradeRecord = new TradeRecord();
                JSONObject data = (JSONObject) x;
                //已有的数据不再爬
                Date tradeTime = DateUtil.parseDate(data.getString("FSRQ"));
                if(lastTradeRecord != null && DateUtil.compare(lastTradeRecord.getTradeTime(),tradeTime) >= 0){
                    break;
                }
                tradeRecord.setTradeTime(tradeTime);
                tradeRecord.setTradePrice(new BigDecimal(data.getString("LJJZ")));
                tradeRecord.setTradeCode(tradeConfig.getTradeCode());
                tradeRecord.setCreateTime(new Date());
                tradeRecords.add(tradeRecord);
            }
            if(ObjectUtil.isNotEmpty(tradeRecords)){
                tradeRecordMapper.insertBatch(tradeRecords);
                count += tradeRecords.size();
            }
            if(tradeRecords.size() < pageSize){
                break;
            }
            pageIndex ++;
        }while (true);

        long end = System.currentTimeMillis();
        if(count > 0){
            TradeCrawlRecord param = new TradeCrawlRecord();
            param.setTradeCode(tradeConfig.getTradeCode());
            param.setCrawledPlace(CrawledPlaceEnum.TTFUND.code);
            param.setCrawlStatus(StatusConstant.Crawle.SUCCESS.code);
            param.setCrawlDuration((int) (end-start));
            param.setCrawlCount(count);
            param.setCreateTime(new Date());
            crawlRecordMapper.insert(param);
        }
        return result;
    }

    @Override
    public PageInfo<TradeRecordDTO> page(TradeRecord param, PageDomain pageDomain) {
        PageHelper.startPage(pageDomain.getPage(), pageDomain.getLimit());
        List<TradeRecordDTO> list = tradeRecordMapper.selectList(param);
        return new PageInfo<>(list);
    }

}




