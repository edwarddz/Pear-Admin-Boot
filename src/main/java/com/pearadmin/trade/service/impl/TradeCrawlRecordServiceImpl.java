package com.pearadmin.trade.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.pearadmin.common.web.domain.request.PageDomain;
import com.pearadmin.trade.pojo.TradeCrawlRecord;
import com.pearadmin.trade.pojo.dto.TradeCrawlRecordDTO;
import com.pearadmin.trade.mapper.TradeCrawlRecordMapper;
import com.pearadmin.trade.service.TradeCrawlRecordService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
* @author lidezhi
* @description 针对表【trade_crawl_record(爬取记录表)】的数据库操作Service实现
* @createDate 2024-07-24 14:50:12
*/
@Slf4j
@Service
public class TradeCrawlRecordServiceImpl implements TradeCrawlRecordService {

    @Resource
    private TradeCrawlRecordMapper tradeCrawlRecordMapper;

    @Override
    public PageInfo<TradeCrawlRecordDTO> page(TradeCrawlRecord param, PageDomain pageDomain) {
        PageHelper.startPage(pageDomain.getPage(), pageDomain.getLimit());
        List<TradeCrawlRecordDTO> list = tradeCrawlRecordMapper.selectList(param);
        return new PageInfo<>(list);
    }

}




