package com.pearadmin.trade.service.impl;

import cn.hutool.core.date.DateUtil;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.pearadmin.common.context.UserContext;
import com.pearadmin.common.web.domain.request.PageDomain;
import com.pearadmin.common.web.domain.response.Result;
import com.pearadmin.trade.constant.CrawledPlaceEnum;
import com.pearadmin.trade.constant.TradePointEnum;
import com.pearadmin.trade.pojo.TradeConfig;
import com.pearadmin.trade.service.TradeConfigService;
import com.pearadmin.trade.mapper.TradeConfigMapper;
import lombok.extern.slf4j.Slf4j;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.io.IOException;
import java.util.Date;
import java.util.List;

/**
* @author lidezhi
* @description 针对表【trade_config(交易爬虫配置表)】的数据库操作Service实现
* @createDate 2023-07-19 18:44:14
*/
@Slf4j
@Service
public class TradeConfigServiceImpl implements TradeConfigService{

    @Resource
    private TradeConfigMapper tradeConfigMapper;


    @Override
    public Result fundInfoData(TradeConfig tradeConfig) {
        Result result = new Result();
        //成功爬取条数计数
        String url = "http://fundf10.eastmoney.com/jjjz_"+tradeConfig.getTradeCode()+".html";
        Document doc = null;
        try {
            doc = Jsoup.connect(url).get();
            //利用选择器快速定位元素，就可以获取其值
            Elements eles = doc.getElementsByClass("bs_gl");	//找到一个元素集合
            Elements ps = eles.get(0).getElementsByTag("p").get(0).children();

            //成立日期
            tradeConfig.setEstablishDate(DateUtil.parseDate(extracted(ps.get(0))));
            //基金管理者/上市公司法人
            tradeConfig.setManager(extracted(ps.get(1)));
            //市值
            String marketValue = extracted(ps.get(4)).replace("亿元","").split("（")[0];
            tradeConfig.setMarketValue(marketValue);

            String bs_jz = doc.getElementsByClass("bs_jz").get(0).text();
            tradeConfig.setTradeName(bs_jz.split("\\(\\d+")[0]);

            tradeConfig.setTradePoint(TradePointEnum.DATE.code);
            tradeConfig.setCrawledPlace(CrawledPlaceEnum.TTFUND.code);
            tradeConfig.setUpdateTime(new Date());
            tradeConfig.setUpdateBy(UserContext.currentUser().getUsername());
            tradeConfigMapper.updateByPrimaryKeySelective(tradeConfig);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }

    private String extracted(Element p) {
        String[] strs = p.text().split("：", -1);
        return strs[1].replaceAll("[\\s　\\u00A0]+","");
    }

    @Override
    public PageInfo<TradeConfig> page(TradeConfig param, PageDomain pageDomain) {
        PageHelper.startPage(pageDomain.getPage(), pageDomain.getLimit());
        List<TradeConfig> list = tradeConfigMapper.selectList(param);
        return new PageInfo<>(list);
    }


}




