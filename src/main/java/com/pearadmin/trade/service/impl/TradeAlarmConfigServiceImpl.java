package com.pearadmin.trade.service.impl;

import com.pearadmin.trade.service.TradeAlarmConfigService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
* @author lidezhi
* @description 针对表【trade_alarm_config(告警算法适用交易配置表)】的数据库操作Service实现
* @createDate 2023-07-19 18:39:03
*/
@Slf4j
@Service
public class TradeAlarmConfigServiceImpl implements TradeAlarmConfigService{

}




