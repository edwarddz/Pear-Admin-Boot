package com.pearadmin.trade.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.pearadmin.common.web.domain.request.PageDomain;
import com.pearadmin.trade.mapper.TradeAlgorithmConfigMapper;
import com.pearadmin.trade.pojo.TradeAlgorithmConfig;
import com.pearadmin.trade.service.TradeAlgorithmConfigService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
* @author lidezhi
* @description 针对表【trade_algorithm_config】的数据库操作Service实现
* @createDate 2024-07-24 14:50:12
*/
@Slf4j
@Service
public class TradeAlgorithmConfigServiceImpl implements TradeAlgorithmConfigService{

    @Resource
    private TradeAlgorithmConfigMapper configMapper;

    @Override
    public PageInfo<TradeAlgorithmConfig> page(TradeAlgorithmConfig param, PageDomain pageDomain) {
        PageHelper.startPage(pageDomain.getPage(), pageDomain.getLimit());
        List<TradeAlgorithmConfig> list = configMapper.selectList(param);
        return new PageInfo<>(list);
    }
}




