package com.pearadmin.trade.controller;

import com.github.pagehelper.PageInfo;
import com.pearadmin.common.web.base.BaseController;
import com.pearadmin.common.web.domain.request.PageDomain;
import com.pearadmin.common.web.domain.response.module.ResultTable;
import com.pearadmin.trade.pojo.TradeRecord;
import com.pearadmin.trade.pojo.dto.TradeRecordDTO;
import com.pearadmin.trade.service.TradeRecordService;
import io.swagger.annotations.Api;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;

/**
 * @Author:lidezhi
 * @DATE: 2023/7/25 10:53
 * Description:
 * @Version 1.0
 */
@RestController
@Api(tags = {"交易历史信息"})
@RequestMapping("traderecord")
public class TradeRecordController extends BaseController {

    @Resource
    private TradeRecordService tradeRecordService;

    /**
     * Describe: 获取列表视图
     * Param ModelAndView
     * Return 列表视图
     */
    @GetMapping("main")
    @PreAuthorize("hasPermission('/traderecord/main','traderecord:main')")
    public ModelAndView main() {
        return jumpPage("tradecrawl/traderecord/main");
    }

    /**
     * Describe: 获取列表数据
     * Param PageDomain
     * Return 列表数据
     */
    @GetMapping("data")
    @PreAuthorize("hasPermission('/traderecord/data','traderecord:data')")
    public ResultTable data(PageDomain pageDomain, TradeRecord param) {
        PageInfo<TradeRecordDTO> pageInfo = tradeRecordService.page(param, pageDomain);
        return pageTable(pageInfo.getList(), pageInfo.getTotal());
    }
}
