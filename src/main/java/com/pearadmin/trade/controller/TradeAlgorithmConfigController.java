package com.pearadmin.trade.controller;

import cn.hutool.core.util.ObjectUtil;
import com.github.pagehelper.PageInfo;
import com.pearadmin.common.context.UserContext;
import com.pearadmin.common.web.base.BaseController;
import com.pearadmin.common.web.domain.request.PageDomain;
import com.pearadmin.common.web.domain.response.Result;
import com.pearadmin.common.web.domain.response.module.ResultTable;
import com.pearadmin.trade.constant.StatusConstant;
import com.pearadmin.trade.constant.TypeEnum;
import com.pearadmin.trade.mapper.TradeAlgorithmConfigMapper;
import com.pearadmin.trade.pojo.TradeAlgorithmConfig;
import com.pearadmin.trade.pojo.TradeConfig;
import com.pearadmin.trade.service.TradeAlgorithmConfigService;
import io.swagger.annotations.Api;
import org.springframework.context.ApplicationContext;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

/**
 * @Author: lidezhi
 * @DATE: 2023/7/21 15:26
 * @Description: 交易信息爬取配置
 * @Version 1.0
 */
@RestController
@Api(tags = {"算法配置"})
@RequestMapping("algorithmconfig")
public class TradeAlgorithmConfigController extends BaseController {

    @Resource
    private TradeAlgorithmConfigService configService;

    @Resource
    private TradeAlgorithmConfigMapper configMapper;

    @Resource
    private ApplicationContext context;

    /**
     * Describe: 获取列表视图
     * Param ModelAndView
     * Return 列表视图
     */
    @GetMapping("main")
    @PreAuthorize("hasPermission('/algorithmconfig/main','algorithmconfig:main')")
    public ModelAndView main(Model model) {
        model.addAttribute("statusNames", StatusConstant.USAGE.getAllToList());
        return jumpPage("tradecrawl/algorithmconfig/main");
    }

    /**
     * Describe: 获取列表数据
     * Param PageDomain
     * Return 列表数据
     */
    @GetMapping("data")
    @PreAuthorize("hasPermission('/algorithmconfig/data','algorithmconfig:data')")
    public ResultTable data(PageDomain pageDomain, TradeAlgorithmConfig param) {
        PageInfo<TradeAlgorithmConfig> pageInfo = configService.page(param, pageDomain);
        return pageTable(pageInfo.getList(), pageInfo.getTotal());
    }

    /**
     * Describe: 获取新增视图
     * Param ModelAndView
     * Return ModelAndView
     */
    @GetMapping("add")
    @PreAuthorize("hasPermission('/algorithmconfig/add','algorithmconfig:add')")
    public ModelAndView add(Model model) {
        model.addAttribute("statusNames", StatusConstant.USAGE.getAllToList());
        return jumpPage("tradecrawl/algorithmconfig/add");
    }

    /**
     * Describe: 获取修改视图
     * Param ModelAndView
     * Return ModelAndView
     */
    @GetMapping("edit")
    @PreAuthorize("hasPermission('/algorithmconfig/edit','algorithmconfig:edit')")
    public ModelAndView edit(Model model, Long configId) {
        model.addAttribute("algorithmConfig", configMapper.selectByPrimaryKey(configId));
        return jumpPage("tradecrawl/algorithmconfig/edit");
    }

    /**
     * Describe: 保存数据
     * Param tradeConfig
     * Return Result
     */
    @RequestMapping("/save")
    @PreAuthorize("hasPermission('/algorithmconfig/add','algorithmconfig:add')")
    public Result save(@RequestBody TradeAlgorithmConfig param) {
        if (checkParam(param)) return decide(0, "", "算法不存在");
        param.setCreateTime(new Date());
        param.setCreateBy(UserContext.currentUser().getUsername());

       int count = configMapper.insert(param);
        return decide(count,"新增成功","新增失败");
    }

    /**
     * Describe: 更新数据
     * Param tradeConfig
     * Return Result
     */
    @RequestMapping("/update")
    @PreAuthorize("hasPermission('/algorithmconfig/edit','algorithmconfig:edit')")
    public Result update(@RequestBody TradeAlgorithmConfig param) {
        if (checkParam(param)) return decide(0, "", "算法不存在");
        param.setUpdateTime(new Date());
        param.setUpdateBy(UserContext.currentUser().getUsername());
        int result = configMapper.updateByPrimaryKeySelective(param);
        return decide(result);
    }

    /**
     * Describe: 删除
     * Param: configId
     * Return Result
     */
    @RequestMapping("/remove/{id}")
    @PreAuthorize("hasPermission('/algorithmconfig/remove','algorithmconfig:remove')")
    public Result deleteJob(@PathVariable("id") Long configId) {
        int result = configMapper.deleteByPrimaryKey(configId);
        return decide(result, "删除成功", "删除失败");
    }

    private boolean checkParam(TradeAlgorithmConfig param) {
        //校验beaId，method 是否存在
        try {
            Object bean = context.getBean(param.getBeanId());
            if(TypeEnum.AlgorithmTypeEnum.SINGLE.typeCode.equals(param.getAlgorithmType())){
                bean.getClass().getMethod(param.getBeanMethod(), new Class[]{TradeConfig.class});
            }else if(TypeEnum.AlgorithmTypeEnum.MAIN.typeCode.equals(param.getAlgorithmType())){
                bean.getClass().getMethod(param.getBeanMethod(), new Class[]{});
            }else{
                return false;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return true;
        }
        return false;
    }

}
