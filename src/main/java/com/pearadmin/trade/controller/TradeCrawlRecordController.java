package com.pearadmin.trade.controller;

import com.github.pagehelper.PageInfo;
import com.pearadmin.common.web.base.BaseController;
import com.pearadmin.common.web.domain.request.PageDomain;
import com.pearadmin.common.web.domain.response.module.ResultTable;
import com.pearadmin.trade.pojo.TradeCrawlRecord;
import com.pearadmin.trade.pojo.dto.TradeCrawlRecordDTO;
import com.pearadmin.trade.service.TradeCrawlRecordService;
import io.swagger.annotations.Api;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;

/**
 * @Author: lidezhi
 * @DATE: 2023/7/25 10:53
 * @Description:
 * @Version 1.0
 */
@RestController
@Api(tags = {"交易信息爬取记录"})
@RequestMapping("crawlrecord")
public class TradeCrawlRecordController extends BaseController {

    @Resource
    private TradeCrawlRecordService crawlRecordService;

    /**
     * Describe: 获取列表视图
     * Param ModelAndView
     * Return 列表视图
     */
    @GetMapping("main")
    @PreAuthorize("hasPermission('/crawlrecord/main','crawlrecord:main')")
    public ModelAndView main() {
        return jumpPage("tradecrawl/crawlrecord/main");
    }

    /**
     * Describe: 获取列表数据
     * Param PageDomain
     * Return 列表数据
     */
    @GetMapping("data")
    @PreAuthorize("hasPermission('/crawlrecord/data','crawlrecord:data')")
    public ResultTable data(PageDomain pageDomain, TradeCrawlRecord param) {
        PageInfo<TradeCrawlRecordDTO> pageInfo = crawlRecordService.page(param, pageDomain);
        return pageTable(pageInfo.getList(), pageInfo.getTotal());
    }
}
