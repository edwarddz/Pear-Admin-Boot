package com.pearadmin.trade.controller;

import cn.hutool.core.util.ObjectUtil;
import com.github.pagehelper.PageInfo;
import com.pearadmin.common.context.UserContext;
import com.pearadmin.common.web.base.BaseController;
import com.pearadmin.common.web.domain.request.PageDomain;
import com.pearadmin.common.web.domain.response.Result;
import com.pearadmin.common.web.domain.response.module.ResultTable;
import com.pearadmin.trade.constant.StatusConstant;
import com.pearadmin.trade.mapper.TradeConfigMapper;
import com.pearadmin.trade.pojo.TradeConfig;
import com.pearadmin.trade.service.TradeConfigService;
import io.swagger.annotations.Api;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import java.util.Date;

/**
 * @Author: lidezhi
 * @DATE: 2023/7/21 15:26
 * @Description: 交易信息爬取配置
 * @Version 1.0
 */
@RestController
@Api(tags = {"交易信息爬取配置"})
@RequestMapping("tradeconfig")
public class TradeConfigController extends BaseController {

    @Resource
    private TradeConfigService tradeConfigService;

    @Resource
    private TradeConfigMapper tradeConfigMapper;

    /**
     * Describe: 获取列表视图
     * Param ModelAndView
     * Return 列表视图
     */
    @GetMapping("main")
    @PreAuthorize("hasPermission('/tradeconfig/main','tradeconfig:main')")
    public ModelAndView main(Model model) {
        model.addAttribute("statusNames", StatusConstant.INTEREST.getAllToList());
        return jumpPage("tradecrawl/tradeconfig/main");
    }

    /**
     * Describe: 获取列表数据
     * Param PageDomain
     * Return 列表数据
     */
    @GetMapping("data")
    @PreAuthorize("hasPermission('/tradeconfig/data','tradeconfig:data')")
    public ResultTable data(PageDomain pageDomain, TradeConfig param) {
        PageInfo<TradeConfig> pageInfo = tradeConfigService.page(param, pageDomain);
        return pageTable(pageInfo.getList(), pageInfo.getTotal());
    }

    /**
     * Describe: 获取新增视图
     * Param ModelAndView
     * Return ModelAndView
     */
    @GetMapping("add")
    @PreAuthorize("hasPermission('/tradeconfig/add','tradeconfig:add')")
    public ModelAndView add(Model model) {
        model.addAttribute("statusNames", StatusConstant.INTEREST.getAllToList());
        return jumpPage("tradecrawl/tradeconfig/add");
    }

    /**
     * Describe: 获取修改视图
     * Param ModelAndView
     * Return ModelAndView
     */
    @GetMapping("edit")
    @PreAuthorize("hasPermission('/tradeconfig/edit','tradeconfig:edit')")
    public ModelAndView edit(Model model, Long configId) {
        model.addAttribute("statusNames", StatusConstant.INTEREST.getAllToList());
        model.addAttribute("tradeConfig", tradeConfigMapper.selectByPrimaryKey(configId));
        return jumpPage("tradecrawl/tradeconfig/edit");
    }

    /**
     * Describe: 保存数据
     * Param tradeConfig
     * Return Result
     */
    @RequestMapping("/save")
    @PreAuthorize("hasPermission('/tradeconfig/add','tradeconfig:add')")
    public Result save(@RequestBody TradeConfig tradeConfig) {
        tradeConfig.setCreateTime(new Date());
        tradeConfig.setCreateBy(UserContext.currentUser().getUsername());
        int count = tradeConfigMapper.insertSelective(tradeConfig);
        tradeConfigService.fundInfoData(tradeConfig);
        return decide(count);
    }

    /**
     * Describe: 执行一次
     * Param configId
     * Return Result 执行结果
     */
    @RequestMapping("/run")
    @PreAuthorize("hasPermission('/tradeconfig/run','tradeconfig:run')")
    public Result run(Long configId) {
        TradeConfig tradeConfig = tradeConfigMapper.selectByPrimaryKey(configId);
        if(ObjectUtil.isNull(tradeConfig)){
            return failure("交易信息不存在");
        }
        tradeConfigService.fundInfoData(tradeConfig);
        return success("运行成功");
    }

    /**
     * Describe: 更新数据
     * Param tradeConfig
     * Return Result
     */
    @RequestMapping("/update")
    @PreAuthorize("hasPermission('/tradeconfig/edit','tradeconfig:edit')")
    public Result update(@RequestBody TradeConfig tradeConfig) {
        tradeConfig.setUpdateTime(new Date());
        tradeConfig.setUpdateBy(UserContext.currentUser().getUsername());
        int result = tradeConfigMapper.updateByPrimaryKeySelective(tradeConfig);
        return decide(result);
    }

    /**
     * Describe: 删除
     * Param: configId
     * Return Result
     */
    @RequestMapping("/remove/{id}")
    @PreAuthorize("hasPermission('/tradeconfig/remove','tradeconfig:remove')")
    public Result deleteJob(@PathVariable("id") Long configId) {
        int result = tradeConfigMapper.deleteByPrimaryKey(configId);
        return decide(result, "删除成功", "删除失败");
    }

}
