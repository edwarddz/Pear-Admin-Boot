package com.pearadmin.trade.constant;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @Author: lidezhi
 * @DATE: 2023/7/20 10:08
 * @Description: 爬取平台
 * @Version 1.0
 */
public enum CrawledPlaceEnum {
    TTFUND(0,"天天基金"),
    EASTMONEY(1,"东方财富"),
    ENIU(2,"亿牛"),
    SNOWBALL(3,"雪球"),
    LEGU(4,"乐咕乐股"),
    HOW2J(5,"how2j"),
    CSINDEX(6,"中证指数");

    public Integer code;

    public String name;

    private CrawledPlaceEnum(Integer code,String name){
        this.code = code;
        this.name = name;
    }

    public static String getName(Integer code){
        CrawledPlaceEnum[] enums = CrawledPlaceEnum.values();
        for (CrawledPlaceEnum anEnum : enums) {
            if(anEnum.code.equals(code)){
                return anEnum.name;
            }
        }
        return null;
    }

    /**
     * 获得所有枚举类型到list
     * @return
     */
    public static List<CrawledPlaceEnum> getAllToList() {
        List<CrawledPlaceEnum> list = new ArrayList<>();
        CrawledPlaceEnum[] values = values();
        Collections.addAll(list, values);
        return list;
    }

}
