package com.pearadmin.trade.constant;

import java.util.*;

/**
 * @Author: lidezhi
 * @DATE: 2023/7/20 10:11
 * @Description: 状态常量
 * @Version 1.0
 */
public class StatusConstant {

    //关注状态
    public enum INTEREST{
        FOCUS(0,"持有"),FOLLOW(1,"关注"),
        UNFOLLOW(2,"取消关注");
        public Integer code;

        public String describe;

        private INTEREST(Integer code,String describe){
            this.code = code;
            this.describe = describe;
        }

        public static String getName(Integer code){
            INTEREST[] enums = INTEREST.values();
            for (INTEREST anEnum : enums) {
                if(anEnum.code.equals(code)){
                    return anEnum.describe;
                }
            }
            return null;
        }
        /**
         * 获得所有枚举类型到list
         * @return
         */
        public static List<INTEREST> getAllToList() {
            List<INTEREST> list = new ArrayList<>();
            INTEREST[] values = values();
            Collections.addAll(list, values);
            return list;
        }
    }

    //爬取状态
    public enum Crawle{
        SUCCESS(0,"成功"),FAIL(1,"失败"),PART_SUCCESS(2,"部分成功");
        public Integer code;

        public String describe;

        private Crawle(Integer code,String describe){
            this.code = code;
            this.describe = describe;
        }

        public static String getName(Integer code){
            Crawle[] enums = Crawle.values();
            for (Crawle anEnum : enums) {
                if(anEnum.code.equals(code)){
                    return anEnum.describe;
                }
            }
            return null;
        }
    }

    //使用状态
    public enum USAGE{
        ACTIVE(0,"使用中"),CANCEL(1,"作废");
        public Integer code;

        public String describe;

        private USAGE(Integer code,String describe){
            this.code = code;
            this.describe = describe;
        }

        public static String getName(Integer code){
            USAGE[] enums = USAGE.values();
            for (USAGE anEnum : enums) {
                if(anEnum.code.equals(code)){
                    return anEnum.describe;
                }
            }
            return null;
        }

        /**
         * 获得所有枚举类型到list
         * @return
         */
        public static List<USAGE> getAllToList() {
            List<USAGE> list = new ArrayList<>();
            USAGE[] values = values();
            Collections.addAll(list, values);
            return list;
        }
    }
}
