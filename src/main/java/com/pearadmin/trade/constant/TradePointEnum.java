package com.pearadmin.trade.constant;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @Author: lidezhi
 * @DATE: 2023/7/20 10:07
 * @Description: 交易时点
 * @Version 1.0
 */
public enum TradePointEnum {
    HOU(0,"时"),
    DATE(1,"日"),
    WEEK(2,"周"),
    MON(3,"月");

    public Integer code;

    public String timeName;

    private TradePointEnum(Integer code,String timeName){
        this.code = code;
        this.timeName = timeName;
    }

    public static String getName(Integer code){
        TradePointEnum[] enums = TradePointEnum.values();
        for (TradePointEnum anEnum : enums) {
            if(anEnum.code.equals(code)){
                return anEnum.timeName;
            }
        }
        return null;
    }

    /**
     * 获得所有枚举类型到list
     * @return
     */
    public static List<TradePointEnum> getAllToList() {
        List<TradePointEnum> list = new ArrayList<>();
        TradePointEnum[] values = values();
        Collections.addAll(list, values);
        return list;
    }
}
