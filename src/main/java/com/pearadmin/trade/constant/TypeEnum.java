package com.pearadmin.trade.constant;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @Author: lidezhi
 * @DATE: 2024/7/26 17:06
 * @Description: 类型枚举集合
 * @Version 1.0
 */
public class TypeEnum {

    /**
     * @Author:lidezhi
     * @DATE: 2023/7/20 10:10
     * Description:算法适用范围(0-单支，1-大类)
     * @Version 1.0
     */
    public enum AlgorithmTypeEnum {
        SINGLE(0,"单支"),
        MAIN(1,"大类");

        public Integer typeCode;

        public String typeName;

        private AlgorithmTypeEnum(Integer typeCode,String typeName){
            this.typeCode = typeCode;
            this.typeName = typeName;
        }

        public static String getTypeName(Integer typeCode){
            AlgorithmTypeEnum[] enums = AlgorithmTypeEnum.values();
            for (AlgorithmTypeEnum anEnum : enums) {
                if(anEnum.typeCode.equals(typeCode)){
                    return anEnum.typeName;
                }
            }
            return null;
        }

        /**
         * 获得所有枚举类型到list
         * @return
         */
        public static List<AlgorithmTypeEnum> getAllToList() {
            List<AlgorithmTypeEnum> list = new ArrayList<>();
            AlgorithmTypeEnum[] values = values();
            Collections.addAll(list, values);
            return list;
        }
    }
    /**
     * @Author:lidezhi
     * @DATE: 2023/7/20 10:10
     * Description:交易类型
     * @Version 1.0
     */
    public enum TradeTypeEnum {
        BONDFUND(0,"债券基金",2,100),
        STOCKFUND(1,"股票基金",2,100),
        MIXFUND(2,"混合基金",2,200),
        GOOGSFUND(3,"商品基金",5,500);

        public Integer typeCode;

        public String typeName;

        public Integer sizeMin;//建议最小值

        public Integer sizeMax;//建议最大值

        private TradeTypeEnum(Integer typeCode,String typeName,Integer sizeMin,Integer sizeMax){
            this.typeCode = typeCode;
            this.typeName = typeName;
            this.sizeMax = sizeMax;
            this.sizeMin = sizeMin;
        }

        public static String getTypeName(Integer typeCode){
            TradeTypeEnum[] enums = TradeTypeEnum.values();
            for (TradeTypeEnum anEnum : enums) {
                if(anEnum.typeCode.equals(typeCode)){
                    return anEnum.typeName;
                }
            }
            return null;
        }

        /**
         * 获得所有枚举类型到list
         * @return
         */
        public static List<TradeTypeEnum> getAllToList() {
            List<TradeTypeEnum> list = new ArrayList<>();
            TradeTypeEnum[] values = values();
            Collections.addAll(list, values);
            return list;
        }
    }

}
