package com.pearadmin.trade.test;

import cn.hutool.core.date.DateUtil;
import com.alibaba.fastjson.JSONObject;
import com.pearadmin.common.util.HttpUtils;
import lombok.extern.slf4j.Slf4j;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @Author:lidezhi
 * @DATE: 2023/7/20 16:33
 * Description:爬取指定编号的基金数据
 * @Version 1.0
 */
@Slf4j
public class FundTest {

    public static void main(String[] args) throws IOException {
        //爬基金信息数据
        fundData("260108");
        //爬基金交易数据
//        tradeAlarm("260108");
    }

    private static void fundData(String fundCode) throws IOException {
        String url = "http://fundf10.eastmoney.com/jjjz_"+fundCode+".html";
        Document doc = Jsoup.connect(url).get();

        //利用选择器快速定位元素，就可以获取其值
        Elements eles = doc.getElementsByClass("bs_gl");	//找到一个元素集合

        for(Element ele : eles){
            Elements ps = ele.getElementsByTag("p").get(0).children();
            for(Element p : ps){
                String[] strs = p.text().split("：", -1);
                String str0 = strs[0].replaceAll("[\\s　\\u00A0]+","");
                String str1 =strs[1].replaceAll("[\\s　\\u00A0]+","");
                System.out.println(str0 + str1);
            }
        }

        String bs_jz = doc.getElementsByClass("bs_jz").get(0).text();
        System.out.println(bs_jz.split("\\(\\d+")[0]);
    }

    private static void tradeData(String fundCode) {
        String url ="http://api.fund.eastmoney.com/f10/lsjz";
        Map<String, String> params = new HashMap<>();
        String callback = "jQuery18308909743577296265_1618718938738";
        params.put("callback",callback);
        params.put("fundCode", fundCode);
        params.put("pageIndex", "1");
        params.put("pageSize", "10");
        params.put("_", new Date().getTime()+"");

        String str = HttpUtils.doGet(url, params);
        str = str.replace(callback,"");
        str = str.replace("(","");
        str = str.replace(")","");

        log.info("当前时间::::" + DateUtil.formatDateTime(new Date()) + ":" + str);
        JSONObject jsonObject = JSONObject.parseObject(str);

        System.out.println("执行成功"+ str);
    }
}
