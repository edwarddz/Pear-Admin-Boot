package com.pearadmin.trade.pojo;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 
 * @TableName trade_algorithm_config
 */
@Data
public class TradeAlgorithmConfig implements Serializable {
    /**
     * 主键
     */
    private Integer id;

    /**
     * 告警算法名称
     */
    private String algorithm;

    /**
     * spring bean
     */
    private String beanId;

    /**
     * 算法对应的方法
     */
    private String beanMethod;

    /**
     * 0-使用中、1-暂停使用
     */
    private Integer algorithmStatus;

    /**
     * 算法适用范围(0-单支，1-大类)
     */
    private Integer algorithmType;

    /**
     * 创建人
     */
    private String createBy;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 修改人
     */
    private String updateBy;

    /**
     * 修改时间
     */
    private Date updateTime;

    /**
     * 算法描述
     */
    private String remark;

    private static final long serialVersionUID = 1L;

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        TradeAlgorithmConfig other = (TradeAlgorithmConfig) that;
        return (this.getId() == null ? other.getId() == null : this.getId().equals(other.getId()))
            && (this.getAlgorithm() == null ? other.getAlgorithm() == null : this.getAlgorithm().equals(other.getAlgorithm()))
            && (this.getBeanId() == null ? other.getBeanId() == null : this.getBeanId().equals(other.getBeanId()))
            && (this.getBeanMethod() == null ? other.getBeanMethod() == null : this.getBeanMethod().equals(other.getBeanMethod()))
            && (this.getAlgorithmStatus() == null ? other.getAlgorithmStatus() == null : this.getAlgorithmStatus().equals(other.getAlgorithmStatus()))
            && (this.getAlgorithmType() == null ? other.getAlgorithmType() == null : this.getAlgorithmType().equals(other.getAlgorithmType()))
            && (this.getCreateBy() == null ? other.getCreateBy() == null : this.getCreateBy().equals(other.getCreateBy()))
            && (this.getCreateTime() == null ? other.getCreateTime() == null : this.getCreateTime().equals(other.getCreateTime()))
            && (this.getUpdateBy() == null ? other.getUpdateBy() == null : this.getUpdateBy().equals(other.getUpdateBy()))
            && (this.getUpdateTime() == null ? other.getUpdateTime() == null : this.getUpdateTime().equals(other.getUpdateTime()))
            && (this.getRemark() == null ? other.getRemark() == null : this.getRemark().equals(other.getRemark()));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getId() == null) ? 0 : getId().hashCode());
        result = prime * result + ((getAlgorithm() == null) ? 0 : getAlgorithm().hashCode());
        result = prime * result + ((getBeanId() == null) ? 0 : getBeanId().hashCode());
        result = prime * result + ((getBeanMethod() == null) ? 0 : getBeanMethod().hashCode());
        result = prime * result + ((getAlgorithmStatus() == null) ? 0 : getAlgorithmStatus().hashCode());
        result = prime * result + ((getAlgorithmType() == null) ? 0 : getAlgorithmType().hashCode());
        result = prime * result + ((getCreateBy() == null) ? 0 : getCreateBy().hashCode());
        result = prime * result + ((getCreateTime() == null) ? 0 : getCreateTime().hashCode());
        result = prime * result + ((getUpdateBy() == null) ? 0 : getUpdateBy().hashCode());
        result = prime * result + ((getUpdateTime() == null) ? 0 : getUpdateTime().hashCode());
        result = prime * result + ((getRemark() == null) ? 0 : getRemark().hashCode());
        return result;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", algorithm=").append(algorithm);
        sb.append(", beanId=").append(beanId);
        sb.append(", beanMethod=").append(beanMethod);
        sb.append(", algorithmStatus=").append(algorithmStatus);
        sb.append(", algorithmType=").append(algorithmType);
        sb.append(", createBy=").append(createBy);
        sb.append(", createTime=").append(createTime);
        sb.append(", updateBy=").append(updateBy);
        sb.append(", updateTime=").append(updateTime);
        sb.append(", remark=").append(remark);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}