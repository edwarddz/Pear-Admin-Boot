package com.pearadmin.trade.pojo.dto;

import com.pearadmin.trade.pojo.TradeRecord;
import lombok.Data;

/**
 * @Author:lidezhi
 * @DATE: 2023/7/21 15:12
 * Description:
 * @Version 1.0
 */
@Data
public class TradeRecordDTO extends TradeRecord {

    /**
     * 基金/股票名称
     */
    private String tradeName;

    /**
     * 交易类型，0-债券基金，1-股票基金，2-股票
     */
    private Integer tradeType;

    /**
     * 交易时点，(0-时，1-日，2-周，3-月)
     */
    private Integer tradePoint;


}
