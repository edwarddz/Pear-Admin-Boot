package com.pearadmin.trade.pojo;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import lombok.Data;

/**
 * 交易爬虫配置表
 * @TableName trade_config
 */
@Data
public class TradeConfig implements Serializable {
    /**
     * 主键
     */
    private Integer id;

    /**
     * 基金/股票名称
     */
    private String tradeName;

    /**
     * 交易编号
     */
    private String tradeCode;

    /**
     * 交易类型，0-债券基金，1-股票基金，2-混合基金，3-商品基金
     */
    private Integer tradeType;

    /**
     * 基金成立日期/股票上市日期
     */
    private Date establishDate;

    /**
     * 基金管理者/上市公司法人
     */
    private String manager;

    /**
     * 市值
     */
    private String marketValue;

    /**
     * 交易时点，(0-时，1-日，2-周，3-月)
     */
    private Integer tradePoint;

    /**
     * 爬取平台(0-天天基金，1-东方财富)
     */
    private Integer crawledPlace;

    /**
     * 规模下限
     */
    private Integer sizeMin;

    /**
     * 规模上限
     */
    private Integer sizeMax;

    /**
     * 占比上限
     */
    private BigDecimal rateMax;

    /**
     * 占比下限
     */
    private BigDecimal rateMin;

    /**
     * 关注状态(0-持有、1-关注、2-取消关注)
     */
    private Integer tradeStatus;

    /**
     * 创建人
     */
    private String createBy;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 修改人
     */
    private String updateBy;

    /**
     * 修改时间
     */
    private Date updateTime;

    /**
     * 备注
     */
    private String remark;

    private static final long serialVersionUID = 1L;

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        TradeConfig other = (TradeConfig) that;
        return (this.getId() == null ? other.getId() == null : this.getId().equals(other.getId()))
            && (this.getTradeName() == null ? other.getTradeName() == null : this.getTradeName().equals(other.getTradeName()))
            && (this.getTradeCode() == null ? other.getTradeCode() == null : this.getTradeCode().equals(other.getTradeCode()))
            && (this.getTradeType() == null ? other.getTradeType() == null : this.getTradeType().equals(other.getTradeType()))
            && (this.getEstablishDate() == null ? other.getEstablishDate() == null : this.getEstablishDate().equals(other.getEstablishDate()))
            && (this.getManager() == null ? other.getManager() == null : this.getManager().equals(other.getManager()))
            && (this.getMarketValue() == null ? other.getMarketValue() == null : this.getMarketValue().equals(other.getMarketValue()))
            && (this.getTradePoint() == null ? other.getTradePoint() == null : this.getTradePoint().equals(other.getTradePoint()))
            && (this.getCrawledPlace() == null ? other.getCrawledPlace() == null : this.getCrawledPlace().equals(other.getCrawledPlace()))
            && (this.getSizeMin() == null ? other.getSizeMin() == null : this.getSizeMin().equals(other.getSizeMin()))
            && (this.getSizeMax() == null ? other.getSizeMax() == null : this.getSizeMax().equals(other.getSizeMax()))
            && (this.getRateMax() == null ? other.getRateMax() == null : this.getRateMax().equals(other.getRateMax()))
            && (this.getRateMin() == null ? other.getRateMin() == null : this.getRateMin().equals(other.getRateMin()))
            && (this.getTradeStatus() == null ? other.getTradeStatus() == null : this.getTradeStatus().equals(other.getTradeStatus()))
            && (this.getCreateBy() == null ? other.getCreateBy() == null : this.getCreateBy().equals(other.getCreateBy()))
            && (this.getCreateTime() == null ? other.getCreateTime() == null : this.getCreateTime().equals(other.getCreateTime()))
            && (this.getUpdateBy() == null ? other.getUpdateBy() == null : this.getUpdateBy().equals(other.getUpdateBy()))
            && (this.getUpdateTime() == null ? other.getUpdateTime() == null : this.getUpdateTime().equals(other.getUpdateTime()))
            && (this.getRemark() == null ? other.getRemark() == null : this.getRemark().equals(other.getRemark()));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getId() == null) ? 0 : getId().hashCode());
        result = prime * result + ((getTradeName() == null) ? 0 : getTradeName().hashCode());
        result = prime * result + ((getTradeCode() == null) ? 0 : getTradeCode().hashCode());
        result = prime * result + ((getTradeType() == null) ? 0 : getTradeType().hashCode());
        result = prime * result + ((getEstablishDate() == null) ? 0 : getEstablishDate().hashCode());
        result = prime * result + ((getManager() == null) ? 0 : getManager().hashCode());
        result = prime * result + ((getMarketValue() == null) ? 0 : getMarketValue().hashCode());
        result = prime * result + ((getTradePoint() == null) ? 0 : getTradePoint().hashCode());
        result = prime * result + ((getCrawledPlace() == null) ? 0 : getCrawledPlace().hashCode());
        result = prime * result + ((getSizeMin() == null) ? 0 : getSizeMin().hashCode());
        result = prime * result + ((getSizeMax() == null) ? 0 : getSizeMax().hashCode());
        result = prime * result + ((getRateMax() == null) ? 0 : getRateMax().hashCode());
        result = prime * result + ((getRateMin() == null) ? 0 : getRateMin().hashCode());
        result = prime * result + ((getTradeStatus() == null) ? 0 : getTradeStatus().hashCode());
        result = prime * result + ((getCreateBy() == null) ? 0 : getCreateBy().hashCode());
        result = prime * result + ((getCreateTime() == null) ? 0 : getCreateTime().hashCode());
        result = prime * result + ((getUpdateBy() == null) ? 0 : getUpdateBy().hashCode());
        result = prime * result + ((getUpdateTime() == null) ? 0 : getUpdateTime().hashCode());
        result = prime * result + ((getRemark() == null) ? 0 : getRemark().hashCode());
        return result;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", tradeName=").append(tradeName);
        sb.append(", tradeCode=").append(tradeCode);
        sb.append(", tradeType=").append(tradeType);
        sb.append(", establishDate=").append(establishDate);
        sb.append(", manager=").append(manager);
        sb.append(", marketValue=").append(marketValue);
        sb.append(", tradePoint=").append(tradePoint);
        sb.append(", crawledPlace=").append(crawledPlace);
        sb.append(", sizeMin=").append(sizeMin);
        sb.append(", sizeMax=").append(sizeMax);
        sb.append(", rateMax=").append(rateMax);
        sb.append(", rateMin=").append(rateMin);
        sb.append(", tradeStatus=").append(tradeStatus);
        sb.append(", createBy=").append(createBy);
        sb.append(", createTime=").append(createTime);
        sb.append(", updateBy=").append(updateBy);
        sb.append(", updateTime=").append(updateTime);
        sb.append(", remark=").append(remark);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}