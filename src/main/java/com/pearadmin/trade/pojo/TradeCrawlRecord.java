package com.pearadmin.trade.pojo;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 爬取记录表
 * @TableName trade_crawl_record
 */
@Data
public class TradeCrawlRecord implements Serializable {
    /**
     * 主键
     */
    private Long id;

    /**
     * 交易编号
     */
    private String tradeCode;

    /**
     * 爬取数据量
     */
    private Integer crawlCount;

    /**
     * 爬取耗时(ms)
     */
    private Integer crawlDuration;

    /**
     * 爬取状态(0-成功、1-失败、2-部分成功)
     */
    private Integer crawlStatus;

    /**
     * 爬取平台(0-天天基金，1-东方财富)
     */
    private Integer crawledPlace;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 备注
     */
    private String remark;

    private static final long serialVersionUID = 1L;

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        TradeCrawlRecord other = (TradeCrawlRecord) that;
        return (this.getId() == null ? other.getId() == null : this.getId().equals(other.getId()))
            && (this.getTradeCode() == null ? other.getTradeCode() == null : this.getTradeCode().equals(other.getTradeCode()))
            && (this.getCrawlCount() == null ? other.getCrawlCount() == null : this.getCrawlCount().equals(other.getCrawlCount()))
            && (this.getCrawlDuration() == null ? other.getCrawlDuration() == null : this.getCrawlDuration().equals(other.getCrawlDuration()))
            && (this.getCrawlStatus() == null ? other.getCrawlStatus() == null : this.getCrawlStatus().equals(other.getCrawlStatus()))
            && (this.getCrawledPlace() == null ? other.getCrawledPlace() == null : this.getCrawledPlace().equals(other.getCrawledPlace()))
            && (this.getCreateTime() == null ? other.getCreateTime() == null : this.getCreateTime().equals(other.getCreateTime()))
            && (this.getRemark() == null ? other.getRemark() == null : this.getRemark().equals(other.getRemark()));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getId() == null) ? 0 : getId().hashCode());
        result = prime * result + ((getTradeCode() == null) ? 0 : getTradeCode().hashCode());
        result = prime * result + ((getCrawlCount() == null) ? 0 : getCrawlCount().hashCode());
        result = prime * result + ((getCrawlDuration() == null) ? 0 : getCrawlDuration().hashCode());
        result = prime * result + ((getCrawlStatus() == null) ? 0 : getCrawlStatus().hashCode());
        result = prime * result + ((getCrawledPlace() == null) ? 0 : getCrawledPlace().hashCode());
        result = prime * result + ((getCreateTime() == null) ? 0 : getCreateTime().hashCode());
        result = prime * result + ((getRemark() == null) ? 0 : getRemark().hashCode());
        return result;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", tradeCode=").append(tradeCode);
        sb.append(", crawlCount=").append(crawlCount);
        sb.append(", crawlDuration=").append(crawlDuration);
        sb.append(", crawlStatus=").append(crawlStatus);
        sb.append(", crawledPlace=").append(crawledPlace);
        sb.append(", createTime=").append(createTime);
        sb.append(", remark=").append(remark);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}