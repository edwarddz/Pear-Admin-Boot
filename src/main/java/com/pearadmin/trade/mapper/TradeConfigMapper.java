package com.pearadmin.trade.mapper;

import com.pearadmin.trade.pojo.TradeConfig;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
* @author lidezhi
* @description 针对表【trade_config(交易爬虫配置表)】的数据库操作Mapper
* @createDate 2024-07-26 15:10:57
* @Entity com.pearadmin.trade.pojo.TradeConfig
*/
@Mapper
public interface TradeConfigMapper {

    int deleteByPrimaryKey(Long id);

    int insert(TradeConfig record);

    int insertSelective(TradeConfig record);

    TradeConfig selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(TradeConfig record);

    int updateByPrimaryKey(TradeConfig record);

    List<TradeConfig> selectList(TradeConfig param);

}
