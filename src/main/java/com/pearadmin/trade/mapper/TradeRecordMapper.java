package com.pearadmin.trade.mapper;

import com.pearadmin.trade.pojo.TradeRecord;
import com.pearadmin.trade.pojo.dto.TradeRecordDTO;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
* @author lidezhi
* @description 针对表【trade_record(交易记录表)】的数据库操作Mapper
* @createDate 2024-07-24 16:00:52
* @Entity com.pearadmin.trade.pojo.TradeRecord
*/
@Mapper
public interface TradeRecordMapper {

    int deleteByPrimaryKey(Long id);

    int insert(TradeRecord record);

    int insertSelective(TradeRecord record);

    TradeRecord selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(TradeRecord record);

    int updateByPrimaryKey(TradeRecord record);

    List<TradeRecordDTO> selectList(TradeRecord param);

    TradeRecord selectBylast(String tradeCode);

    int insertBatch(List<TradeRecord> tradeRecords);
}
