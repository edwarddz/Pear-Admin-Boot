package com.pearadmin.trade.mapper;

import com.pearadmin.trade.pojo.TradeAlarmConfig;
import org.apache.ibatis.annotations.Mapper;

/**
* @author lidezhi
* @description 针对表【trade_alarm_config(告警算法适用交易配置表)】的数据库操作Mapper
* @createDate 2024-07-24 16:00:52
* @Entity com.pearadmin.trade.pojo.TradeAlarmConfig
*/
@Mapper
public interface TradeAlarmConfigMapper {

    int deleteByPrimaryKey(Long id);

    int insert(TradeAlarmConfig record);

    int insertSelective(TradeAlarmConfig record);

    TradeAlarmConfig selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(TradeAlarmConfig record);

    int updateByPrimaryKey(TradeAlarmConfig record);

}
