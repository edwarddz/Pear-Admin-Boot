package com.pearadmin.trade.mapper;

import com.pearadmin.trade.pojo.TradeAlgorithmConfig;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
* @author lidezhi
* @description 针对表【trade_algorithm_config】的数据库操作Mapper
* @createDate 2024-07-29 08:59:48
* @Entity com.pearadmin.trade.pojo.TradeAlgorithmConfig
*/
@Mapper
public interface TradeAlgorithmConfigMapper {

    int deleteByPrimaryKey(Long id);

    int insert(TradeAlgorithmConfig record);

    int insertSelective(TradeAlgorithmConfig record);

    TradeAlgorithmConfig selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(TradeAlgorithmConfig record);

    int updateByPrimaryKey(TradeAlgorithmConfig record);

    List<TradeAlgorithmConfig> selectList(TradeAlgorithmConfig record);

}
