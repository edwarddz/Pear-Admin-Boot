package com.pearadmin.trade.mapper;

import com.pearadmin.trade.pojo.TradeCrawlRecord;
import com.pearadmin.trade.pojo.dto.TradeCrawlRecordDTO;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
* @author lidezhi
* @description 针对表【trade_crawl_record(爬取记录表)】的数据库操作Mapper
* @createDate 2024-07-24 16:00:52
* @Entity com.pearadmin.trade.pojo.TradeCrawlRecord
*/
@Mapper
public interface TradeCrawlRecordMapper {

    int deleteByPrimaryKey(Long id);

    int insert(TradeCrawlRecord record);

    int insertSelective(TradeCrawlRecord record);

    TradeCrawlRecord selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(TradeCrawlRecord record);

    int updateByPrimaryKey(TradeCrawlRecord record);

    List<TradeCrawlRecordDTO> selectList(TradeCrawlRecord param);
}
