package com.pearadmin.trade.schedule;

import cn.hutool.core.date.DateUtil;
import com.pearadmin.common.quartz.base.BaseQuartz;
import com.pearadmin.trade.constant.CrawledPlaceEnum;
import com.pearadmin.trade.constant.StatusConstant;
import com.pearadmin.trade.mapper.TradeConfigMapper;
import com.pearadmin.trade.pojo.TradeConfig;
import com.pearadmin.trade.service.TradeConfigService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @Author:lidezhi
 * @DATE: 2023/7/17 10:49
 * Description:爬取天天基金-基金基础信息
 * @Version 1.0
 */
@Slf4j
@Component("fundInfoData")
public class FundInfoData implements BaseQuartz {

    @Resource
    private TradeConfigMapper tradeConfigMapper;

    @Resource
    private TradeConfigService tradeConfigService;

    @Override
    public void run(String params) throws Exception {
        log.info("爬取天天基金基础信息开始,Params === >> " + params);
        TradeConfig tradeConfig = new TradeConfig();
        tradeConfig.setCrawledPlace(CrawledPlaceEnum.TTFUND.code);
        tradeConfig.setTradeCode(params);
        List<TradeConfig> tradeConfigs = tradeConfigMapper.selectList(tradeConfig);
        tradeConfigs = tradeConfigs.stream().
                filter(x -> x.getTradeStatus() != StatusConstant.INTEREST.UNFOLLOW.code).collect(Collectors.toList());

        tradeConfigs.forEach(x -> {
            tradeConfigService.fundInfoData(x);
        });

        log.info("当前时间::::" + DateUtil.formatDateTime(new Date()) + ":");
    }
}
